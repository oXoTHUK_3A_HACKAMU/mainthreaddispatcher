﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainThreadDispetcher : MonoBehaviour
{
    public static MainThreadDispetcher Instance;
    private Queue<Action> QueuedActions = new Queue<Action>();
    private void Start()
    {
        Instance = this;
    }
    private void Update()
    {
        while (QueuedActions.Count > 0)
        {
            QueuedActions.Dequeue().Invoke();
        }
    }
    public void InvokeIsMainThread(Action act)
    {
        QueuedActions.Enqueue(act);
    }
}
